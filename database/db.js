const Sequelize = require("sequelize")
const db = {}
const sequelize = new Sequelize("tap_india", "root", "", {
    host: "localhost",
    dialect: "mysql",
    operatorsAlaises: false,

    pool : {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db