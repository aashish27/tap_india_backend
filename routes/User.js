const express = require("express")
const users = express.Router()
const cors = require("cors")
const jwt = require("jsonwebtoken")
const bcrypt = require("bcrypt")

const Users = require("../models/Users")
users.use(cors())

process.env.SECRET_KEY = "secret"

//REGISTER
users.post('/register', (req, res)=>{
    const today = new Date()
    const userData = {
        first_name : req.body.first_name,
        last_name : req.body.last_name,
        email : req.body.email,
        type: req.body.type,
        password : req.body.password,
        createdAt : today,
        updatedAt : req.body.updatedAt
    }

    Users.findOne({
        where: {
            email: req.body.email
        }
    }).then(user=>{
        if(!user){
            const hash = bcrypt.hashSync(userData.password, 10)
            userData.password = hash
            Users.create(userData).then(user=>{
                let token = jwt.sign(user.dataValues, process.env.SECRET_KEY,{
                    expiresIn: 1440
                })
                res.json({token})
            })
            .catch(err=>{
                res.send("Error: "+ err)
            })
        }else{
            res.json({error: "User already exists"})
        }
    }).catch(err=>{
        res.send("Error: "+ err)
    })
})


//LOGIN
users.post('/login', (req, res)=>{
    Users.findOne({
        where: {
            email: req.body.email
        }
    }).then(user=>{
        if(bcrypt.compareSync(req.body.password, user.password)){
            let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                expiresIn: 1440
            })
            res.json({token, message: "Login Successful!", success: true})
        }else{
            res.send("User does not exist")
        }
    }).catch(err=>{
        res.send("Error: "+ err)
    })
})


//PROFILE
users.get('/profile', (req, res)=>{
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

    Users.findOne({
        where: {
            id: decoded.id
        }
    }).then(user=>{
        if(user){
            res.json(user)
        }else{
            res.send("User does not exist")
        }
    }).catch(err=>{
        res.send("Error: "+ err)
    })
})

module.exports = users